﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using fatsecret.platform;
using Newtonsoft.Json;

namespace fatsecret_test_app
{
    internal static class Program
    {
        private const string BaseUrl = FatSecretAPI.URL_BASE;
        private const string ConsumerKey = "72c0f387c91f49fbbac24cd133d8524e";
        private const string ConsumerSecret = "c5eeb088117a456fa6d64221b3da4c59";
        private const string FoodGetMethod = "food.get";
        private const string AccessToken = "";

        private static void Main()
        {
            var countries = new List<string> {"nl", "be", "es", "ie"};

            var foodItemsNl = new Dictionary<int, string>
            {
                {21620771, "Nourish for Life Natuurlijke Rijke Bessenaroma’s"},
                {21620755, "Nourish for Life - Natural Rich Berry Flavour"},
                {21620757, "Ionix Supreme - Natural Fruit Flavour"},
                {21620773, "Ionix Supreme Natuurlijke Druivensmaak"},
                {21620779, "IsaLean Shake Romige Aardbeiensmaak"},
                {21620765, "IsaMove"},
                {21620763, "IsaLean Shake - Creamy Strawberry Flavour"},
                {21620814, "Snacks Chocoladesmaak"},
                {21620767, "Snacks - Chocolate Flavour"}
            };

            var foodItemsBe = new Dictionary<int, string>
            {
                {21620957, "Ionix Supreme Natuurlijke Druivensmaak"},
                {21620952, "Snacks - Chocolate Flavour"},
                {21620955, "Nourish for Life Natuurlijke Rijke Bessenaroma’s"},
                {21620942, "Ionix Supreme - Natural Fruit Flavour"},
                {21620947, "IsaLean Shake - Creamy Strawberry Flavour"},
                {21620970, "Nourish for Life Saveur Fruits Rouges Naturel"},
                {21620972, "Ionix Supreme Saveur Naturel de Fruit"},
                {21620964, "IsaLean Shake Romige Aardbeiensmaak"},
                {21620983, "Snacks Saveur Chocolat"},
                {21620967, "Snacks Chocoladesmaak"},
                {21620940, "Nourish for Life - Natural Rich Berry Flavour"},
                {21620949, "IsaMove"},
                {21620978, "IsaLean Shake Saveur Fraise Crémeuse"},
                {21620980, "IsaMove"}
            };

            var foodItemsEs = new Dictionary<int, string>
            {
                {21621052, "IsaMove"},
                {21621050, "IsaLean Shake - Creamy Strawberry Flavour"},
                {21621043, "Nourish for Life - Natural Rich Berry Flavour"},
                {21621055, "Snacks - Chocolate Flavour"},
                {21621045, "Ionix Supreme - Natural Fruit Flavour"}
            };

            var foodItemsIe = new Dictionary<int, string>
            {
                {21620718, "Nourish for Life - Natural Rich Berry Flavour"},
                {21620729, "Snacks - Chocolate Flavour"},
                {21620725, "IsaLean Shake - Creamy Strawberry Flavour"},
                {21620720, "Ionix Supreme - Natural Fruit Flavour"},
                {21620727, "IsaMove"}
            };

            foreach (var country in countries)
            {
                Dictionary<int, string> foodItems;
                switch (country.ToUpper())
                {
                    case "nl":
                        foodItems = foodItemsNl;
                        GenerateJson(country, foodItems);
                        break;
                    case "be":
                        foodItems = foodItemsBe;
                        GenerateJson(country, foodItems);
                        break;
                    case "es":
                        foodItems = foodItemsEs;
                        GenerateJson(country, foodItems);
                        break;
                    case "ie":
                        foodItems = foodItemsIe;
                        GenerateJson(country, foodItems);
                        break;
                }
            }

            Console.WriteLine("Done");
        }

        private static void GenerateJson(string country, Dictionary<int, string> foodItems)
        {
            var docs = new List<string>();

            foreach (var foodItem in foodItems)
            {
                var url = GenerateUrl(foodItem.Key, country);

                using (var client = new WebClient())
                {
                    docs.Add(client.DownloadString(url));
                }
            }

            foreach (var doc in docs)
            {
                if (docs.IndexOf(doc) == 0)
                {
                    using (var file =
                        File.CreateText(
                            $"/Users/jace.babin/RiderProjects/fatsecret_test_app/fatsecret_test_app/fatsecret.{country}.json")
                    )
                    {
                        var data = JsonConvert.DeserializeObject(doc);
                        var serializer = new JsonSerializer();

                        file.Write("[");
                        serializer.Serialize(file, data);
                        file.Write(",");
                    }
                }
                else if (docs.IndexOf(doc) == docs.Count - 1)
                {
                    using (var file =
                        File.AppendText(
                            $"/Users/jace.babin/RiderProjects/fatsecret_test_app/fatsecret_test_app/fatsecret.{country}.json")
                    )
                    {
                        var data = JsonConvert.DeserializeObject(doc);
                        var serializer = new JsonSerializer();

                        serializer.Serialize(file, data);
                        file.Write("]");
                    }
                }
                else
                {
                    using (var file =
                        File.AppendText(
                            $"/Users/jace.babin/RiderProjects/fatsecret_test_app/fatsecret_test_app/fatsecret.{country}.json")
                    )
                    {
                        var data = JsonConvert.DeserializeObject(doc);
                        var serializer = new JsonSerializer();

                        serializer.Serialize(file, data);
                        file.Write(",");
                    }
                }
            }
        }

        private static string GenerateUrl(int foodItem, object region)
        {
            var url =
                $"{BaseUrl}format=json&method={FoodGetMethod}&oauth_consumer_key={ConsumerKey}&food_id={foodItem}&region={region}";

            var signatureBase = GenerateSignatureBase(new Uri(url),
                WebUtility.UrlEncode(ConsumerKey), AccessToken,
                "GET", GenerateTimeStamp(), GenerateNonce(),
                "HMAC-SHA1", out var normalizedUrl, out var normalizedRequestParameters);

            //get the signature
            var signature = FatSecretService.GetAuthSignature(signatureBase, UrlEncode(ConsumerSecret), "");

            var pasteUrl = string.Concat(normalizedUrl, "?", normalizedRequestParameters, "&oauth_signature=",
                WebUtility.UrlEncode(signature));

            return pasteUrl;
        }

        private static string GenerateSignatureBase(Uri url, string consumerKey, string token, string httpMethod,
            string timeStamp, string nonce, string signatureType, out string normalizedUrl,
            out string normalizedRequestParameters)
        {
            normalizedUrl = null;
            normalizedRequestParameters = null;
            IList result = new List<QueryParameter>();
            GetQueryParameters(url.Query, result);
            result.Add(new QueryParameter("oauth_version", "1.0"));
            result.Add(new QueryParameter("oauth_nonce", nonce));
            result.Add(new QueryParameter("oauth_timestamp", timeStamp));
            result.Add(new QueryParameter("oauth_signature_method", signatureType));
            result.Add(new QueryParameter("oauth_consumer_key", consumerKey));
            if (!string.IsNullOrEmpty(token))
            {
                result.Add(new QueryParameter("oauth_token", token));
            }

            ((List<QueryParameter>) result).Sort(new QueryParameterComparer());
            normalizedUrl = string.Format("{0}://{1}", url.Scheme, url.Host);
            if (((url.Scheme != "http") || (url.Port != 80)) && ((url.Scheme != "https") || (url.Port != 0x1bb)))
            {
                normalizedUrl = normalizedUrl + ":" + url.Port;
            }

            normalizedUrl = normalizedUrl + url.AbsolutePath;
            normalizedRequestParameters = NormalizeRequestParameters(result);
            var encodedNormalizedParameters = UrlEncode(normalizedRequestParameters);
            var builder = new StringBuilder();
            builder.AppendFormat("{0}&", httpMethod);
            builder.AppendFormat("{0}&", UrlEncode(normalizedUrl));
            builder.AppendFormat("{0}", encodedNormalizedParameters);
            return builder.ToString();
        }

        private static string UrlEncode(string value)
        {
            const string unreservedChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_.~";
            var builder = new StringBuilder();
            foreach (var ch in value)
            {
                if (unreservedChars.IndexOf(ch) != -1)
                {
                    builder.Append(ch);
                }
                else
                {
                    builder.Append('%' + string.Format("{0:X2}", (int) ch));
                }
            }

            return builder.ToString();
        }

        private static string NormalizeRequestParameters(IList parameters)
        {
            var builder = new StringBuilder();
            for (var i = 0; i < parameters.Count; i++)
            {
                var parameter = (QueryParameter) parameters[i];
                builder.AppendFormat("{0}={1}", parameter.Name, parameter.Value);
                if (i < (parameters.Count - 1))
                {
                    builder.Append("&");
                }
            }

            return builder.ToString();
        }

        private static void GetQueryParameters(string parameters, IList result)
        {
            if (parameters.StartsWith("?", StringComparison.CurrentCultureIgnoreCase))
            {
                parameters = parameters.Remove(0, 1);
            }

            if (string.IsNullOrEmpty(parameters)) return;
            foreach (var str in parameters.Split(new[] {'&'}))
            {
                // Doesnt start with oauth_ (except for oauth_token, which is required for profile.get.
                if (string.IsNullOrEmpty(str) || str.StartsWith("oauth_", StringComparison.CurrentCultureIgnoreCase) ||
                    str.StartsWith("xoauth_", StringComparison.CurrentCultureIgnoreCase) ||
                    str.StartsWith("opensocial_", StringComparison.CurrentCultureIgnoreCase)) continue;
                if (str.IndexOf('=') > -1)
                {
                    var strArray2 = str.Split(new[] {'='});
                    result.Add(new QueryParameter(strArray2[0], strArray2[1]));
                }
                else
                {
                    result.Add(new QueryParameter(str, string.Empty));
                }
            }
        }

        private static string GenerateNonce()
        {
            return Guid.NewGuid().ToString().Replace("-", "");
        }

        private static string GenerateTimeStamp()
        {
            var span = DateTime.UtcNow - new DateTime(0x7b2, 1, 1, 0, 0, 0, 0);
            return Convert.ToInt64(span.TotalSeconds).ToString();
        }
    }

    internal class QueryParameterComparer : IComparer<QueryParameter>, IComparer
    {
        // Methods
        public int Compare(object a, object b)
        {
            var parameter = (QueryParameter) a;
            var parameter2 = (QueryParameter) b;
            return parameter.Name == parameter2.Name
                ? string.CompareOrdinal(parameter.Value, parameter2.Value)
                : string.CompareOrdinal(parameter.Name, parameter2.Name);
        }

        #region IComparer<QueryParameter> Members

        public int Compare(QueryParameter x, QueryParameter y)
        {
            return Compare(x, (object) y);
        }

        #endregion
    }

    internal class QueryParameter
    {
        // Fields

        // Methods
        public QueryParameter(string name, string value)
        {
            Name = name;
            Value = value;
        }

        // Properties
        public string Name { get; }

        public string Value { get; }
    }
}