﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace fatsecret_test_app
{
    public class FatSecretService 
    {

        #region OAuth Parameters

        //public const string OAUTH_VERSION_NUMBER = "1.0";
        //public const string OAUTH_PARAMETER_PREFIX = "oauth_";
        //public const string XOAUTH_PARAMETER_PREFIX = "xoauth_";
        //public const string OPEN_SOCIAL_PARAMETER_PREFIX = "opensocial_";

        //public const string OAUTH_CONSUMER_KEY = "oauth_consumer_key";
        //public const string OAUTH_CALLBACK = "oauth_callback";
        //public const string OAUTH_VERSION = "oauth_version";
        //public const string OAUTH_SIGNATURE_METHOD = "oauth_signature_method";
        //public const string OAUTH_SIGNATURE = "oauth_signature";
        //public const string OAUTH_TIMESTAMP = "oauth_timestamp";
        //public const string OAUTH_NONCE = "oauth_nonce";
        //public const string OAUTH_TOKEN = "oauth_token";
        //public const string OAUTH_TOKEN_SECRET = "oauth_token_secret";

        #endregion

        /// <summary>
        /// Gets the auth signature.
        /// </summary>
        /// <returns>The auth signature.</returns>
        /// <param name="signature">Signature.</param>
        /// <param name="consumerSecret">Consumer secret.</param>
        /// <param name="tokenSecret">Token secret.</param>
        public static string GetAuthSignature(string signature, string consumerSecret, string tokenSecret)
        {
            var authObj = new OAuthBase();
            var signatureBase = authObj.GenerateSignature(signature, consumerSecret, tokenSecret);
            return signatureBase;
        }

        private class OAuthBase
        {
            /// <summary>
            /// Computes the hash.
            /// </summary>
            /// <returns>The hash.</returns>
            /// <param name="hashAlgorithm">Hash algorithm.</param>
            /// <param name="data">Data.</param>
            private static string ComputeHash(HashAlgorithm hashAlgorithm, string data)
            {
                var bytes = Encoding.GetEncoding("ASCII").GetBytes(data);
                return Convert.ToBase64String(hashAlgorithm.ComputeHash(bytes));
            }

            /// <summary>
            /// Generates the signature.
            /// </summary>
            /// <returns>The signature.</returns>
            /// <param name="signatureBase">Signature base.</param>
            /// <param name="consumerSecret">Consumer secret.</param>
            /// <param name="tokenSecret">Token secret.</param>
            public string GenerateSignature(string signatureBase, string consumerSecret, string tokenSecret)
            {
                var hash = new HMACSHA1
                {
                    Key = Encoding.GetEncoding("ASCII").GetBytes(string.Format("{0}&{1}", consumerSecret,
                        string.IsNullOrEmpty(tokenSecret) ? "" : tokenSecret))
                };
                return GenerateSignatureUsingHash(signatureBase, hash);
            }

            /// <summary>
            /// Generates the signature using hash.
            /// </summary>
            /// <returns>The signature using hash.</returns>
            /// <param name="signatureBase">Signature base.</param>
            /// <param name="hash">Hash.</param>
            private static string GenerateSignatureUsingHash(string signatureBase, HashAlgorithm hash)
            {
                return ComputeHash(hash, signatureBase);
            }

        }

    }
}